export const listService = [
    {
        title: "Web Apps Development",
        img: "../../assets/images/service_1.jpg"
    },
    {
        title: "Mobile Application Development",
        img: "../../assets/images/service_2.jpg"
    },
    {
        title: "Software System Maintenance",
        img: "../../assets/images/service_3.jpg"
    }
];
export const listAccomplishment = [
    {
        img: "../../assets/images/dcs-cover-thumb.5c1e01e.78409846562adf64270031d6508112b4.jpg",
        sub: "Omnichannels sales system",
        title: "DCS",
    },
    {
        img: "../../assets/images/cms-cover-thumb.5c1e01e.583cb8ae90d7961d2212d36dc0d1dfa7.jpg",
        sub: "CMSContractor Website.",
        title: "CMS Contractor",
    },
    {
        img: "../../assets/images/scn-cover-thumb.5c1e01e.c66dbc88844a7bcf3df9b10a55af7e34.jpg",
        sub: "Omnichannels sales system",
        title: "SCN",
    }
]
export const listBussiness = [
    {
        img: "../../assets/images/gioi-thieu-phan-mem-can-xe-dien-tu-thumb.jpg",
        sub: "Overview of truck weighing software",
        title: "What is weighing software?"
    },
    {
        img: "../../assets/images/nhung-loai-phan-mem-can-xe-thumb.jpg",
        sub: "Introducing two popular weighing software",
        title: "Types of weighing software"
    },
    {
        img: "../../assets/images/cac-loai-dau-can-xe-pho-bien-thumb.jpg",
        sub: "Introducing some popular truck scales",
        title: "Popular types of truck scales"
    }
]
export const listClientSay = [
    {
        fullname: "Ms. An Nguyen",
        message: "The solution provided by GENEAT actually increased our sales by 500%, far exceeding our initial expectations. Great teamwork and collaboration.",
        job: "Founder - DCS Store",
        img: "../../assets/images/person_1.jpg"
    },
    {
        fullname: "Mr. Robin Appleton-Power",
        message: "Possessing expert knowledge of .NET, web services and DEVOPS, GENEAT has a flair for putting together a system design on all levels.",
        job: "Director - WebExaminer",
        img: "../../assets/images/person_4.jpg"
    },
    {
        fullname: "Mr. Tung Hoang",
        message: "Possessing expert knowledge of .NET, web services and DEVOPS, GENEAT has a flair for putting together a system design on all levels.",
        job: "Director - WebExaminer",
        img: "../../assets/images/person_5.jpg"
    }
]

export const listPortfolio = [
    {
        img: "/./assets/images/dcs-cover-thumb.5c1e01e.78409846562adf64270031d6508112b4.jpg",
        sub: "Multiplatform sales network.",
        title: "DCS",
    },
    {
        img: "/./assets/images/saleslikezone-cover-thumb.5c1e01e.a1caf2bfbb3cdda2b2696dcb958c71d8.jpg",
        sub: "LikeZone Sales System",
        title: "Omnichannels sales system",
    },
    {
        img: "/./assets/images/scn-cover-thumb.5c1e01e.c66dbc88844a7bcf3df9b10a55af7e34.jpg",
        sub: "Omnichannels sales system",
        title: "SCN",
    },
    {
        img: "/./assets/images/stup-cover-thumb.5c1e01e.aec396f6a8d277d220f99203ad6e96ec.jpg",
        sub: "A startup for financial",
        title: "Stup.vn",
    },
    {
        img: "/./assets/images/webexaminer-cover-thumb.5c1e01e.a6ec73c614d7cd85e916371a80e4a7b3.jpg",
        sub: "Website for Webexaminer",
        title: "WebExaminer"
    },
    {
        img: "/./assets/images/cms-cover-thumb.5c1e01e.583cb8ae90d7961d2212d36dc0d1dfa7.jpg",
        sub: "Omnichannels sales system",
        title: "SCN",
    },
    {
        img: "/./assets/images/diglo-cover-thumb.5c1e01e.90410fda48fda0f889099d949d897f03.jpg",
        sub: "Solution for sensory needs.",
        title: "DIGLO",
    },
    {
        img: "/./assets/images/likezoneweb-cover-thumb.5c1e01e.221a864ab4dd50cfa061e036a76fc144.jpg",
        sub: "Technology distribution center.",
        title: "LIKEZONE WEBSITE",
    },
    {
        img: "/./assets/images/geneatv2-cover-thumb.5c1e01e.430dc9fc3eae9cf6863e575d60f9f39a.jpg",
        sub: "Made by Gridsome and tailwindcss.",
        title: "GENEAT WEBSITE V2",
    }
]