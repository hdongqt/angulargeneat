import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-photo',
  templateUrl: './card-photo.component.html',
  styleUrls: ['./card-photo.component.css']
})
export class CardPhotoComponent implements OnInit {

  @Input() img?:string;
  @Input() title?:string;
  @Input() sub?:string;
  @Input() link?:string;
  constructor() { }

  ngOnInit(): void {
  }

}
