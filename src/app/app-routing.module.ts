import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlogComponent } from './pages/blog/blog.component';
import { HomeComponent } from './pages/home/home.component';
import { PortfolioDetailComponent } from './pages/portfolio/portfolio-detail/detail.component';
import { PortfolioComponent } from './pages/portfolio/portfolio-main/portfolio.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { 
    path: 'portfolio', 
    children: [
      {
        path: "",
        component: PortfolioComponent
      },
      {
        path: ":slug",
        component: PortfolioDetailComponent
      }
    ]
  },
  { path: 'blog', component: BlogComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
