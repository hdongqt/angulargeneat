import { Component, OnInit } from '@angular/core';
import { listService,listAccomplishment,listBussiness, listClientSay } from './../../app.model';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  listService = listService;
  listAccomplishment = listAccomplishment
  listBussiness = listBussiness
  listClientSay = listClientSay
  constructor() { }

  ngOnInit(): void {
  }

}
