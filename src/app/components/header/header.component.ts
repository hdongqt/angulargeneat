import { Component, OnInit,OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit,OnChanges {

  toggleNavMobile = true;

  constructor() { }

  ngOnInit(): void {
    console.log(this.toggleNavMobile)
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log("dsd")
  }
  toogle(){
    this.toggleNavMobile = !this.toggleNavMobile
    console.log(this.toggleNavMobile)
  }
}
