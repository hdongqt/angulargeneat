import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { CardPhotoComponent } from './components/card-photo/card-photo.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { AppRoutingModule } from './app-routing.module';
import { NavigateComponent } from './components/navigate/navigate.component';
import { BlogComponent } from './pages/blog/blog.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { PortfolioDetailComponent } from './pages/portfolio/portfolio-detail/detail.component';
import { PortfolioComponent } from './pages/portfolio/portfolio-main/portfolio.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    CardPhotoComponent,
    CarouselComponent,
    PortfolioComponent,
    NavigateComponent,
    BlogComponent,
    PaginationComponent,
    PortfolioDetailComponent
  ],
  imports: [
  BrowserModule,
  AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
